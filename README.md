Trumpventure
=============

This game was made on a gamejam at ["Games for change"](http://www.gamesforchange.org/). Read more about it [here](http://us1.campaign-archive1.com/?u=aa9b4edf4a633020c94db2c88&id=a48cc90fe2).

We might continue this project at some time in the future but the final built is already submitted.

**Our ranking in the contest**

We made our game at the [Grafisch Lyceum Utrecht](http://glu.nl/) and there we got 3rd place!


A list of people who worked on this project
------------
Zoë van den Berg (Artist)

Xander Stuivenberg (Artist)

Youri van den Broek (Developer)

Rick van Sloten (Developer)

Short game resume
---
Your are basically a Mexican running away from trump.

You have energy (blue bar) which you can use to run and a health bar that indicated how much health you have left.

Collect all 8 of the bricks to win the game.

