﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class StartScreen
    {
        GameEngine _gameEngine;
        Bitmap _startScreen;
        Button _buttonStart;
        Button _buttonQuit;
        bool _clickBait;

        public StartScreen()
        {
            _clickBait = false;
            _gameEngine = GameEngine.GetInstance();
            _startScreen = new Bitmap("Startscreen.png");
            _buttonStart = new Button(Start, "", 750, 400, 500, 220);
            _buttonQuit = new Button(Quit, "", 750, 705, 500, 220);
        }

        public void Dispose()
        {
            _startScreen.Dispose();
        }

        public bool GetClick()
        {
            return _clickBait;
        }

        public void Start()
        {
            _clickBait = true;
        }

        public void DisableButtons()
        {
            _buttonStart.SetActive(false);
            _buttonQuit.SetActive(false);
        }

        public void EnableButtons()
        {
            _buttonQuit.SetActive(true);
            _buttonStart.SetActive(true);
        }

        public void Update()
        {
            _buttonQuit.ShowBackground(false);
            _buttonQuit.ShowForeground(false);
            _buttonQuit.ShowBorder(false);
            _buttonStart.ShowBackground(false);
            _buttonStart.ShowForeground(false);
            _buttonStart.ShowBorder(false);
        }
        
        public void Quit()
        {
            Environment.Exit(0);
        }

        public void Paint()
        {
            _gameEngine.DrawBitmap(_startScreen, 0, 0);
        }
    }
}
