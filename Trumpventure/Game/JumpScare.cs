﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class JumpScare
    {
        GameEngine _gameEngine;
        Bitmap[] _jumpScare = new Bitmap[4];
        Audio _10feat;
        float _timer = 0.036f; //
        int _state = 0;
        bool _show = false;
        public JumpScare()
        {
            _gameEngine = GameEngine.GetInstance();
            _10feat = new Audio("10feat.mp3");
            _jumpScare[0] = new Bitmap("jumpscare/1.png");
            _jumpScare[1] = new Bitmap("jumpscare/2.png");
            _jumpScare[2] = new Bitmap("jumpscare/3.png");
            _jumpScare[3] = new Bitmap("jumpscare/4.png"); 
        }

        public void Play()
        {
            if (!_show)
            {
                _gameEngine.PlayAudio(_10feat);
            }
            _show = true;
            if (_timer <= 0)
            {
                _timer = 0.036f;
                _state++;
                if (_state > 3)
                {
                    _state = 0;
                }
            }
            _timer -= _gameEngine.GetDeltaTime();
        }

        public void Draw()
        {
            if (_show)
            {
                _gameEngine.DrawBitmap(_jumpScare[_state] , 0, 0);
            }
        }
    }
}
