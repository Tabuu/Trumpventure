﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    public class Trumpventure : AbstractGame
    {
        JumpScare _jumpScare;
        StartScreen _startScreen;
        Player _player;
        Enviorment _enviorment;
        Trump _trump;
        Radar _radar;
        Bricks _bricks;
        Bitmap blurBitmap;
        Bitmap bigBlurBitmap;

        Audio _creapy;

        int _collectedBricks = 0;
        bool _clickBait;
        float _counter;

        Bitmap
            _trump1,
            _trump2,
            _trump3,
            _trump4;

        bool _showingTrump1;
        bool _showingTrump2;
        bool _showingTrump3;
        bool _showingTrump4;


        public override void GameStart()
        {
            _jumpScare = new JumpScare();
            _creapy = new Audio("creapy.mp3");
            _radar = new Radar();
            blurBitmap = new Bitmap("blur.png");
            bigBlurBitmap = new Bitmap("blurBig.png");
            GAME_ENGINE.SetBackgroundColor(Color.Black);
            ///_____
            _bricks = new Bricks();
            _enviorment = new Enviorment();
            _player = new Player(_enviorment.GetGeneratedPlayerLocation());
            _trump = new Trump(_enviorment.GetGeneratedTrumpLocation());
            _startScreen = new StartScreen();
            _counter = 0;

            _trump1 = new Bitmap("1.png");
            _trump2 = new Bitmap("2.png");
            _trump3 = new Bitmap("3.png");
            _trump4 = new Bitmap("4.png");

            _showingTrump1 = false;
            _showingTrump2 = false;
            _showingTrump3 = false;
            _showingTrump4 = false;

            _startScreen.EnableButtons();


            _creapy.SetLooping(true);

            GAME_ENGINE.PlayAudio(_creapy);

            _bricks.Generate();

        }

        public override void GameEnd()
        {
            _startScreen.Dispose();
            _trump1.Dispose();
            _trump2.Dispose();
            _trump3.Dispose();
            _trump4.Dispose();
        }

        public override void Update()
        {
            float deltatime = GAME_ENGINE.GetDeltaTime();

            if (_trump.GetKillTimer() <= 0)
            {
                _jumpScare.Play();
            }

            if(_collectedBricks == 2)
            {
                _showingTrump1 = true;
                _counter += deltatime;
                if(_counter >= 0.3f)
                {
                    _showingTrump1 = false;
                }
            }

            else if(_collectedBricks == 4)
            {
                _showingTrump2 = true;
                _counter += deltatime;
                if(_counter >= 0.3f)
                {
                    _showingTrump2 = false;
                }
            }

            else if(_collectedBricks == 5)
            {
                _showingTrump3 = true;
                _counter += deltatime;
                if(_counter >= 0.3f)
                {
                    _showingTrump3 = false;
                }
            }

            else if(_collectedBricks == 7)
            {
                _showingTrump4 = true;
                _counter += deltatime;
                if(_counter >= 0.3f)
                {
                    _showingTrump4 = false;
                }
            }

            else
            {
                _counter = 0;
            }

            _player.Update(_enviorment.CheckCollision(_player.GetLocation()));
            _trump.Update(_player.GetLocation());
            _enviorment.Update(_player.GetLocation(), _trump.GetLocation());

            for (int i = 0; i < _bricks.GetBricksLeft(); i++)
            {
                float a = _player.GetLocation().Y - _bricks.GetLocation(i).Y;
                float b = _player.GetLocation().X - _bricks.GetLocation(i).X;
                float c = (float)Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
                float tobrick = c;

                if (tobrick <= 64)
                {
                    _collectedBricks++;
                    _bricks.Remove(i);
                }
                _clickBait = _startScreen.GetClick();
                _startScreen.Update();
                if (_clickBait == true)
                {
                    _startScreen.DisableButtons();
                }
            }
        }

        public override void Paint()
        {
            

            _enviorment.Draw();
            for (int i = 0; i < _bricks.GetBricksLeft(); i++)
            {
                _bricks.Paint(i, _player.GetLocation());
            }
            _trump.Draw(_enviorment.GetTrumpOffset());

            GAME_ENGINE.DrawBitmap(blurBitmap, 0, 0);
            
            GAME_ENGINE.SetColor(Color.White);
            /*
            GAME_ENGINE.DrawString("X: " + _player.GetLocation().X + " Y: " + _player.GetLocation().Y, 0, 0, 1000, 1000);
            GAME_ENGINE.DrawString("X: " + _trump.GetLocation().X + " Y: " + _trump.GetLocation().Y, 0, 15, 1000, 1000);

            GAME_ENGINE.DrawString("" + _player.GetMovementSpeed(), 0, 40, 100, 100);
            */
            GAME_ENGINE.DrawString(_collectedBricks + " out of 8 brick collected", 0, 100, 100, 100);
            

            _player.Paint();
            GAME_ENGINE.SetColor(Color.Red);
            GAME_ENGINE.FillRectangle(0, 0, (int)1920 / 4 * _trump.GetKillTimer(), 5);

            _radar.Draw(_bricks.GetLocation(0), _player.GetLocation());
            _jumpScare.Draw();

            if (_clickBait == false)
            {
                _startScreen.Paint();
            }

            if(_showingTrump1 == true)
            {
                GAME_ENGINE.DrawBitmap(_trump1, 0, 0);
            }

            if (_showingTrump2 == true)
            {
                GAME_ENGINE.DrawBitmap(_trump2, 0, 0);
            }

            if (_showingTrump3 == true)
            {
                GAME_ENGINE.DrawBitmap(_trump3, 0, 0);
            }

            if (_showingTrump4 == true)
            {
                GAME_ENGINE.DrawBitmap(_trump4, 0, 0);
            }

        }
    }
}
