﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Player
    {
        Stamina _stamina;
        GameEngine _gameEngine;
        Vector2f _locations;
        float _movementSpeed;
        List<int> _bricks;

        int state = 0;
        float stateTimer = 0;

        Bitmap _playerBitmap;

        Direction _direction;

        enum Direction
        {
            up = 0,
            upRight = 1,
            right = 2,
            downRight = 3,
            down = 4,
            downLeft = 5,
            left = 6,
            upLeft = 7,
        }

        public Player(Vector2f locations)
        {
            _stamina = new Stamina();
            _playerBitmap = new Bitmap("character_strip5.png");
            _gameEngine = GameEngine.GetInstance();
            _locations = new Vector2f(locations.X, locations.Y);
            _movementSpeed = 250;
            _bricks = new List<int>();
        }

        public float GetMovementSpeed()
        {
            return _movementSpeed;
        }

        public void ItemPickUp()
        {
            _bricks.Add(1);
        }

        public void GameEnd()
        {

        }


        bool up;
        bool down;
        bool left;
        bool right;
        bool shift;
        public void Update(bool collision)
        {
            shift = _gameEngine.GetKey(Key.ShiftKey);

            if (!collision)
            {
                up = _gameEngine.GetKey(Key.Up);
                down = _gameEngine.GetKey(Key.Down);
                left = _gameEngine.GetKey(Key.Left);
                right = _gameEngine.GetKey(Key.Right);
            }

            if (stateTimer >= 0.05f)
            {
                stateTimer = 0;
                state++;
                if (state > 4)
                {
                    state = 0;
                }
            }
            stateTimer += _gameEngine.GetDeltaTime();


            float deltatime = _gameEngine.GetDeltaTime();
            float stamina = _stamina.GetStamina();
            //Sprinting
            if (_gameEngine.GetKey(Key.ShiftKey) && stamina > 0)
            {
                _movementSpeed = 350;
            }

            
        
            

            //Sneaking
            else if (_gameEngine.GetKey(Key.ControlKey))
            {
                _movementSpeed = 125;
            }

            else
            {
                _movementSpeed = 250;
            }

            //StaminaDraining
            if (shift && up)
            {
                _stamina.DrainStamina();
            }

            else if (shift && down)
            {
                _stamina.DrainStamina();
            }

            else if (shift && left)
            {
                _stamina.DrainStamina();
            }

            else if (shift && right)
            {
                _stamina.DrainStamina();
            }

            else
            {
                _stamina.RegenStamina();
            }

            if (!collision)
            {
                //Movement
                if (up) _locations.Y -= _movementSpeed * _gameEngine.GetDeltaTime();
                if (down) _locations.Y += _movementSpeed * _gameEngine.GetDeltaTime();
                if (left) _locations.X -= _movementSpeed * _gameEngine.GetDeltaTime();
                if (right) _locations.X += _movementSpeed * _gameEngine.GetDeltaTime();
            }
            else
            {
                if (up) _locations.Y += _movementSpeed * 2 * _gameEngine.GetDeltaTime();
                if (down) _locations.Y -= _movementSpeed * 2 * _gameEngine.GetDeltaTime();
                if (left) _locations.X += _movementSpeed * 2 * _gameEngine.GetDeltaTime();
                if (right) _locations.X -= _movementSpeed * 2 * _gameEngine.GetDeltaTime();
            }

            if (up)
            {
                _direction = Direction.up;
                if (right) _direction = Direction.upRight;
                else if (left) _direction = Direction.upLeft;
                else _direction = Direction.up;
            }
            else if (down)
            {
                if (right) _direction = Direction.downRight;
                else if (left) _direction = Direction.downLeft;
                else _direction = Direction.down;
            }
            else if (left)
            {
                _direction = Direction.left;
            }
            else if (right)
            {
                _direction = Direction.right;
            }
        }
    
        public Vector2f GetLocation()
        {
            return _locations;
        }

        public void Paint()
        {
            //_gameEngine.FillRectangle(1920 / 2, 1080 / 2, 32, 32);
            


            _gameEngine.SetRotation(0.0f);

            switch (_direction)
            {
                case Direction.up:
                    _gameEngine.SetRotation(0.0f);
                    break;
                case Direction.upRight:
                    _gameEngine.SetRotation(45.0f);
                    break;
                case Direction.right:
                    _gameEngine.SetRotation(90.0f);
                    break;
                case Direction.downRight:
                    _gameEngine.SetRotation(135.0f);
                    break;
                case Direction.down:
                    _gameEngine.SetRotation(180.0f);
                    break;
                case Direction.downLeft:
                    _gameEngine.SetRotation(225.0f);
                    break;
                case Direction.left:
                    _gameEngine.SetRotation(270.0f);
                    break;
                case Direction.upLeft:
                    _gameEngine.SetRotation(315.0f);
                    break;
            }

            if (up || down || left || right)
            {
                _gameEngine.DrawBitmap(_playerBitmap, (1920 / 2) - 16, (1080 / 2) - 16, 32 * state, 0, 32, 32);
            }else
            {
                _gameEngine.DrawBitmap(_playerBitmap, (1920 / 2) - 16, (1080 / 2) - 16, 32 * 4, 0, 32, 32);
            }
            
            _gameEngine.ResetRotation();
            _stamina.Paint();
        }
    }
}
