﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Trump
    {
        GameEngine _gameEngine;
        Random _random;

        Vector2f
            _location,
            _velocity,
            _steering,
            _target,
            _playerLocation;

        float
            _detectionRange = 500,
            _attackRange = 150,
            _killTimer = 4,
            _speed = 260;


        float _animationTimer;
        int _animationState;

        float _distanceToPlayer;

        Bitmap
            _trumpBitmap,
            _trump1,
            _trump2,
            _trump3;

        public Trump(Vector2f spawnLocation)
        {
            _gameEngine = GameEngine.GetInstance();
            _random = new Random();

            _trumpBitmap = new Bitmap("trumps_head.png");
            _trump1 = new Bitmap("trump/trump1.png");
            _trump2 = new Bitmap("trump/trump2.png");
            _trump3 = new Bitmap("trump/trump3.png");

            _location = spawnLocation;
        }

        public void Update(Vector2f playerLocation)
        {
            _playerLocation = playerLocation;

            if (_animationTimer <= 0)
            {
                _animationTimer = 0.1f;
                _animationState++;
                if (_animationState > 2)
                {
                    _animationState = 0;
                }
            }
            _animationTimer -= _gameEngine.GetDeltaTime();


            float a = playerLocation.Y - _location.Y;
            float b = playerLocation.X - _location.X;
            float c = (float)Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            _distanceToPlayer = c;

            Vector2f DesiredVelocity = Multiply(Normalize(_target - _location), _speed);
            _steering = DesiredVelocity - _velocity;
            if (Length(_steering) > 1)
            {
                _steering = Multiply(Normalize(_steering), 100);
            }

            _velocity += _steering;

            if (Length(_velocity) > 100)
            {
                _velocity = Multiply(Normalize(_velocity), _speed);
            }
            _target = playerLocation;

            if (_distanceToPlayer <= _detectionRange)
            {
                _location += Multiply(_velocity, _gameEngine.GetDeltaTime());
            }

            if (_distanceToPlayer <= _attackRange)
            {
                _killTimer -= _gameEngine.GetDeltaTime();   
            }

            if (_distanceToPlayer >= 1500)
            {
                _location.X = _random.Next((int)_playerLocation.X, (int)_playerLocation.X + 2000);
                _location.Y = _random.Next((int)_playerLocation.Y, (int)_playerLocation.Y + 2000);
            }

            if (_distanceToPlayer > _attackRange)
            {
                _killTimer = 4.0f;
            }

        }

        private bool Detection()
        {
            if (_distanceToPlayer <= _detectionRange)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Draw(Vector2f drawLocation)
        {
            switch (_animationState)
            {
                case 0:
                    _trumpBitmap = _trump1;
                    break;
                case 1:
                    _trumpBitmap = _trump2;
                    break;
                case 2:
                    _trumpBitmap = _trump3;
                    break;
            }

            //_gameEngine.DrawRectangle(drawLocation.X, drawLocation.Y, 10, 10);

            _gameEngine.SetRotation((float)Math.Acos((_playerLocation.X - _location.X) / _distanceToPlayer));
            float jemoeder = (float)Math.Acos(((_playerLocation.X - _location.X) / _distanceToPlayer));
            _gameEngine.SetScale(2.0f, 2.0f);
            _gameEngine.DrawBitmap(_trumpBitmap, drawLocation);
            _gameEngine.ResetScale();
            _gameEngine.ResetRotation();
        }

        public float GetKillTimer()
        {
            return _killTimer;
        }

        public Vector2f GetLocation()
        {
            return _location;
        }

        public Vector2f Normalize(Vector2f vector)
        {
            float length = Length(vector);

            vector.X /= length;
            vector.Y /= length;
            return vector;
        }

        // multiply a vector with a scaler value
        public Vector2f Multiply(Vector2f vector, float scaler)
        {
            vector.X = vector.X * scaler;
            vector.Y = vector.Y * scaler;
            return vector;
        }

        // get the length of a vector
        public float Length(Vector2f vector)
        {
            float length = (float)System.Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y));
            return length > 0 ? length : 0;
        }
    }
}
