﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Bricks
    {
        GameEngine _gameEngine;
        Random _rnd;
        List<Vector2f> _bricks;

        Bitmap _brickBitmap;

        public Bricks()
        {
            _gameEngine = GameEngine.GetInstance();
            _bricks = new List<Vector2f>();
            _brickBitmap = new Bitmap("brick.png");
            _rnd = new Random();
        }

        public Vector2f GetLocation(int brick)
        {
            try
            {
                return _bricks[brick];
            }
            catch
            {
                return new Vector2f(0, 0);
            }
        }

        public int GetBricksLeft()
        {
            return _bricks.Count;
        }

        public void Generate()
        {
            for (int i = 0; i < 8; i++)
            {
                int x = _rnd.Next(0, 5000);
                int y = _rnd.Next(0, 5000);
                _bricks.Add(new Vector2f(x, y));
            }
        }

        public void Update()
        {

        }

        public void Remove(int brick)
        {
            _bricks.RemoveAt(brick);
        }

        public void Paint(int brick, Vector2f playerLocation)
        {
            Vector2f drawLocation = new Vector2f();
            drawLocation.X = ((playerLocation.X + (1920 / 2)) - _bricks[brick].X);
            drawLocation.Y = ((playerLocation.Y + (1080 / 2)) - _bricks[brick].Y);

            drawLocation.Y += ((1080 / 2) - drawLocation.Y) * 2;
            drawLocation.X += ((1920 / 2) - drawLocation.X) * 2;

            _gameEngine.DrawBitmap(_brickBitmap, drawLocation);
        }
    }
}
