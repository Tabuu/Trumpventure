﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Map
    {
        GameEngine _gameEngine;

        int _width, _height;
        public int[][] mapTiles { get { return _map; } }
        int[][] _map;

        Vector2f _playerLocation;
        Vector2f _trupLocation;

        Bitmap floor1Bitmap = new Bitmap("stone_tile.png");
        Bitmap floor2Bitmap = new Bitmap("stone_tile_2.png");
        Bitmap tree1Bitmap = new Bitmap("tree_a_kleur.png");
        Bitmap tree2Bitmap = new Bitmap("tree_b_kleur.png");
        Bitmap bushBitmap = new Bitmap("tree_b_kleur.png");

        enum tileNumber
        {
            floor1 = 0,
            floor2 = 1,
            tree1 = 2,
            tree2 = 3,
            bush = 4
        }

        Random _rnd = new Random();

        public bool CheckCollision(Vector2f playerLocation)
        {
            //mapTiles[0][0] = 2;
            for (int widthi = 0; widthi < _width; widthi++)
            {
                for (int heighti = 0; heighti < _height; heighti++)
                {
                    int tile = mapTiles[widthi][heighti];
                    if (tile == 2 || tile == 3)
                    {
                        if (playerLocation.X > (widthi * 64) && playerLocation.X < ((widthi * 64) + 64)
                            && playerLocation.Y > (heighti * 64) && playerLocation.Y < (heighti * 64) + 64)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public Map(int width, int height, int treeChance)
        {
            _gameEngine = GameEngine.GetInstance();
            _playerLocation = new Vector2f();
            _trupLocation = new Vector2f();
            _map = new int[width][];
            
            _width = width;
            _height = height;


            for (int i = 0; i < _width; i++)
            {
                _map[i] = new int[_height];
            }

            Generate(treeChance);
            GeneratePlayerLocation();
            GenerateTrumpLocation();

        }

        private void Generate(int treeChance)
        {
            for (int widthi = 0; widthi < _width; widthi++)
            {
                for (int heighti = 0; heighti < _height; heighti++)
                {
                    int precent = _rnd.Next(0, 10000);
                    if (precent <= treeChance)
                    {
                        int chance = _rnd.Next(0, 100);
                        if (chance > 10)
                        {
                            _map[widthi][heighti] = (int)tileNumber.tree1;
                        }
                        else if (chance > 45)
                        {
                            _map[widthi][heighti] = (int)tileNumber.tree2;
                        }
                        else
                        {
                            _map[widthi][heighti] = (int)tileNumber.bush;
                        }
                        //_map[widthi][heighti] = (int)tileNumber.tree1;

                    }

                    else
                    {
                        int chance = _rnd.Next(0, 2);
                        if (chance == 0)
                        {
                            _map[widthi][heighti] = (int)tileNumber.floor2;
                        }
                        else
                        {
                            _map[widthi][heighti] = (int)tileNumber.floor1;
                        }
                    }

                    if (widthi == 0 || heighti == 0 || widthi == 159 || heighti == 159)
                    {
                        _map[widthi][heighti] = (int)tileNumber.tree1;
                    }
                }
            }
        }

        public void Draw(Vector2f offset)
        {           
            if (_gameEngine.GetKeyDown(Key.R))
            {
                Generate(1000);
            }

            for (int widthi = 0; widthi < _width; widthi++)
            {
                for (int heighti = 0; heighti < _height; heighti++)
                {
                    float drawWidth = (widthi * 64) + offset.X;
                    float drawHeight = (heighti * 64) + offset.Y;

                    if (drawWidth <= 1920 && drawHeight <= 1080 && drawWidth >= -32 && drawHeight >= -32)
                    {
                        _gameEngine.SetScale(2.01f, 2.01f);
                        switch (_map[widthi][heighti])
                        {
                            case (int)tileNumber.tree1:
                                _gameEngine.DrawBitmap(floor1Bitmap, drawWidth, drawHeight);
                                _gameEngine.DrawBitmap(tree1Bitmap, drawWidth, drawHeight - 64);
                                break;
                            case (int)tileNumber.floor1:
                                _gameEngine.DrawBitmap(floor1Bitmap, drawWidth, drawHeight);
                                break;
                            case (int)tileNumber.floor2:
                                _gameEngine.DrawBitmap(floor2Bitmap, drawWidth, drawHeight);
                                break;
                            case (int)tileNumber.tree2:
                                _gameEngine.DrawBitmap(floor1Bitmap, drawWidth, drawHeight);
                                _gameEngine.DrawBitmap(tree2Bitmap, drawWidth, drawHeight);
                                break;
                            case (int)tileNumber.bush:
                                _gameEngine.DrawBitmap(floor1Bitmap, drawWidth, drawHeight);
                                _gameEngine.DrawBitmap(bushBitmap, drawWidth, drawHeight);
                                break;
                        }
                        _gameEngine.ResetScale();
                    }                  
                }
            }
        }

        private void GeneratePlayerLocation()
        {
            _playerLocation.X = _rnd.Next(0, _width * 64);
            _playerLocation.Y = _rnd.Next(0, _height * 64);
        }

        private void GenerateTrumpLocation()
        {
            _trupLocation.X = _rnd.Next(0, _width * 64);
            _trupLocation.Y = _rnd.Next(0, _height * 64);
        }

        public Vector2f GetPlayerLocation()
        {
            return _playerLocation;
        }

        public Vector2f GetTrumpLocation()
        {
            return _trupLocation;
        }
    }
}
