﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Enviorment
    {
        GameEngine _gameEngine;

        Map _map;
        Vector2f _playerOffset;
        Vector2f _trumpOffset;

        public Enviorment()
        {
            _gameEngine = GameEngine.GetInstance();
            _map = new Map(160, 160, 1000);
            _playerOffset = new Vector2f(0, 0);
            _trumpOffset = new Vector2f(0, 0);
        }

        public bool CheckCollision(Vector2f playerLocation)
        {
            return _map.CheckCollision(playerLocation);
        }

        public Vector2f GetGeneratedPlayerLocation()
        {
            return _map.GetPlayerLocation();
        }

        public Vector2f GetGeneratedTrumpLocation()
        {
            return _map.GetTrumpLocation();
        }

        public Map GetMap()
        {
            return _map;
        }

        public void Draw()
        {          
            _map.Draw(_playerOffset);
        }

        public Vector2f GetTrumpOffset()
        {
            return _trumpOffset;
        }

        public void Update(Vector2f playerLocation , Vector2f trumpLocation)
        {

            _playerOffset.X = (1920 / 2) - playerLocation.X;
            _playerOffset.Y = (1080 / 2) - playerLocation.Y;

            _trumpOffset.X = ((playerLocation.X + (1920 / 2)) - trumpLocation.X);
            _trumpOffset.Y = ((playerLocation.Y + (1080 / 2)) - trumpLocation.Y);

            _trumpOffset.Y += ((1080 / 2) - _trumpOffset.Y) * 2;
            _trumpOffset.X += ((1920 / 2) - _trumpOffset.X) * 2;
        }
    }
}
