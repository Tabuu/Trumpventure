﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{ 
    class Radar
    {
        GameEngine _gameEngine;
        Bitmap _outline;
        Bitmap _flash;

        public Radar()
        {
            _flash = new Bitmap("frame_radar.png");
            _outline = new Bitmap("radar_groen_ding.png");
            _gameEngine = GameEngine.GetInstance();
        }

        public void Update()
        {

        }

        public void Draw(Vector2f drawlocation, Vector2f playerLocation)
        {
            _gameEngine.SetColor(Color.Black);
            _gameEngine.FillRectangle(_gameEngine.GetScreenWidth() - 490, _gameEngine.GetScreenHeight() - 490, 480, 480);
            //_gameEngine.DrawBitmap(_outline, _gameEngine.GetScreenWidth() - 250, _gameEngine.GetScreenHeight() - 250);
            _gameEngine.DrawBitmap(_flash, _gameEngine.GetScreenWidth() - 500, _gameEngine.GetScreenHeight() - 500);
            _gameEngine.SetColor(Color.Red);
            //_gameEngine.FillEllipse(drawlocation.X / 10 + _gameEngine.GetScreenWidth() - 490, drawlocation.Y / 10 + _gameEngine.GetScreenHeight() - 490, 10, 10);
            //_gameEngine.DrawString("" + drawlocation.X, drawlocation.X / 10 + _gameEngine.GetScreenWidth() - 490, drawlocation.Y / 10 + _gameEngine.GetScreenHeight() - 490, 10, 10);

            Vector2f realdrawLocation = new Vector2f();
            realdrawLocation.X = ((playerLocation.X - (1920 / 2)) - drawlocation.X);
            realdrawLocation.Y = ((playerLocation.Y - (1080 / 2)) - drawlocation.Y);
            realdrawLocation.X = (realdrawLocation.X - realdrawLocation.X * 2) * 10;
            realdrawLocation.Y = (realdrawLocation.Y - realdrawLocation.Y * 2) * 10;

            //realdrawLocation.Y += ((1080 / 2) * 10 - realdrawLocation.Y) * 2;
            //realdrawLocation.X += ((1920 / 2) * 10 - realdrawLocation.X) * 2;

            //_gameEngine.DrawBitmap(_brickBitmap, drawLocation);
            //_gameEngine.FillEllipse(realdrawLocation.X, realdrawLocation.Y, 10, 10);

            
            _gameEngine.SetColor(Color.Red);
            _gameEngine.FillEllipse((drawlocation.X / 20) + (_gameEngine.GetScreenWidth() - 500), (drawlocation.Y / 20) + (_gameEngine.GetScreenHeight() - 500), 10, 10);
            _gameEngine.SetColor(Color.Green);
            _gameEngine.FillEllipse((playerLocation.X / 20) + (_gameEngine.GetScreenWidth() - 500), (playerLocation.Y / 20) + (_gameEngine.GetScreenHeight() - 500), 10, 10);
        }
    }
}
