﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameEngine
{
    class Stamina
    {
        GameEngine _gameEngine;
        float _staminaCounter;
        float _counter;
        Bitmap _stamina1;
        Bitmap _stamina2;
        Bitmap _stamina3;
        Bitmap _stamina4;
        Bitmap _staminaBar;

        public Stamina()
        {
            _gameEngine = GameEngine.GetInstance();
            _staminaCounter = 300;
            _counter = 0;
            _stamina1 = new Bitmap("Stamina1.png");
            _stamina2 = new Bitmap("Stamina2.png");
            _stamina3 = new Bitmap("Stamina3.png");
            _stamina4 = new Bitmap("Stamina4.png");
            _staminaBar = new Bitmap("Staminabalk.png");
        }

        public void Dispose()
        {
            _stamina1.Dispose();
            _stamina2.Dispose();
            _stamina3.Dispose();
            _stamina4.Dispose();
            _staminaBar.Dispose();
        }

        public float GetStamina()
        {
            return _staminaCounter;
        }

        public void DrainStamina()
        {
            float deltaTime = _gameEngine.GetDeltaTime();
            _staminaCounter -= 25 * deltaTime;
        }

        public void RegenStamina()
        {
            float deltaTime = _gameEngine.GetDeltaTime();

            if (_staminaCounter < 100)
            {
                _counter = _counter + deltaTime;
            }

            if(_counter >= 1)
            {
                _staminaCounter += 25 * deltaTime;
                if(_staminaCounter >= 100)
                {
                    _counter = 0;
                    _staminaCounter = 100;
                }
            }
        }


        public void Paint()
        {
            _gameEngine.DrawBitmap(_staminaBar, 0, 40);

            if(_staminaCounter > 0)
            {
                _gameEngine.DrawBitmap(_stamina4, 7, 45);
            }

            if(_staminaCounter > 75)
            {
                _gameEngine.DrawBitmap(_stamina3, 30, 45);
            }

            if(_staminaCounter > 150)
            {
                _gameEngine.DrawBitmap(_stamina2, 53, 45);
            }

            if(_staminaCounter > 225)
            {
                _gameEngine.DrawBitmap(_stamina1, 76, 45);
            }
        }
    }
}
